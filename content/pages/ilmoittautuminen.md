Title: Ilmoittautuminen
Illustration: ilmo_web.jpg


Maisemapyöräilyn ennakkohinta xx EUR on voimassa 31.7.2022 asti. Sen jälkeen ilmoittautuminen maksaa YY EUR. Voit ilmoittautua ja maksaa  myös vasta paikan päällä, mutta hintaan kuuluvien tarjoilujen määrän arvioimiseksi toivomme, että siinäkin tapauksessa tekisit alustavan ilmoittautumisen osoitteeseen ilmo2022@maisemapyoraily.fi


Voit ilmoittautua maksamalla ilmoittautumismaksun allaolevasta linkistä. Maksun jälkeen saat vahvistuksen sähköpostiisi joka toimii pääsylippuna tapahtumaan.

[Ilmoittaudu tästä!](https://buy.stripe.com/test_00g3df6fm81O2hq000)








 


