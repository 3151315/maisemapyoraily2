Title: Kolin Maisemapyöräily
URL:
save_as: index.html
status: hidden


Tervetuloa Suomen kauneimmalle maastopyöräretkelle Kolin upeisiin maisemiin!
==================================

Tapahtumaan ovat tervetulleita kaikenlaiset polkupyörät - sähköavustuksella ja ilman. 6km - 30km pitkistä reiteistä löytyy jokaiselle mieluinen. Tapahtumassa ei ole ajanottoa. Ilmoittautuminen avautuu toukokuussa.

Yhteistyössä:
-------------

[Kolin Ipatti Ry](http://www.ipatti.fi)

[Vanhan Koulun majatalo](http://www.vanhankoulunmajatalo.fi)

[Seikkailuyhtiö Vaara Oy](http://www.vaara.net)

[Kolin Aamuranta](http://www.kolinaamuranta.fi)
 

