Title: Kumppanit
Illustration: kumppanit_web.jpg



Yhteistyökumppaniksi Kolin maisemapyöräilyyn?
=============================================

Tapahtuman avuksi mahtuu vielä yhteistyökumppaneita. Yhteistyö voi olla luonteeltaan taloudellista tai tapahtuman käytännön järjestelyihin osallistumista. Jos olet kiinnostunut, niin ota yhteyttä sähköpostilla <kumppanit2022@maisemapyoraily.fi>




